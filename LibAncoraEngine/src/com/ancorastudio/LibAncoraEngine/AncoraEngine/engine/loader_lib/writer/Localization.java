package com.ancorastudio.LibAncoraEngine.AncoraEngine.engine.loader_lib.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Localization {
	
	private HashMap<String, String> localization;
	
	public Localization(String path) {
		localization = new HashMap<>();
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				String[] temp = reader.nextLine().split(":");
				localization.put(temp[0], temp[1].trim());
			}
		} catch (FileNotFoundException e) {	e.printStackTrace(); }
	}

	public String get(String key) {
		return localization.get(key);
	}
}

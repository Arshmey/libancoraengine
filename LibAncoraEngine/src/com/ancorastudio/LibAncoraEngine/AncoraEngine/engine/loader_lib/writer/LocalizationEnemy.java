package com.ancorastudio.LibAncoraEngine.AncoraEngine.engine.loader_lib.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class LocalizationEnemy {

	private HashMap<String, String> localization_e;
	
	public LocalizationEnemy(String path) {
		localization_e = new HashMap<>();
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				String[] temp = reader.nextLine().split(":");
				localization_e.put(temp[0], temp[1].trim());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String get(String key) {
		return localization_e.get(key);
	}
	
}

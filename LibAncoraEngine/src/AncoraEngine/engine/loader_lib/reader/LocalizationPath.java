package AncoraEngine.engine.loader_lib.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LocalizationPath {

	private String d_Path = "";
	private String[] path_localization;
	
	public LocalizationPath(String path) {
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				d_Path += reader.nextLine();
				path_localization = d_Path.split("path_lang: ");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String[] getElement() {
		return path_localization;
	}

}

package com.ancorastudio.LibAncoraEngine.AncoraEngine.engine.loader_lib.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Image {

	private String writer = "";
	private String[] image;
	
	public Image(String path) {
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				writer += reader.nextLine();
				image = writer.split("img: ");
				System.out.print(writer+"|");
			}
			System.out.println();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String[] getElement() {
		return image;
	}
	
}

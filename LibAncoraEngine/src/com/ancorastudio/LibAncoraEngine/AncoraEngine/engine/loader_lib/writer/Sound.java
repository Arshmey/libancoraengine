package com.ancorastudio.LibAncoraEngine.AncoraEngine.engine.loader_lib.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Sound {

	private HashMap<String, String> sound;
	
	public Sound(String path) {
		sound = new HashMap<>();
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				String[] temp = reader.nextLine().split(":");
				sound.put(temp[0], temp[1].trim());
			}
			System.out.println();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public String get(String key) {
		return sound.get(key);
	}
	
}

package AncoraEngine.engine.loader_lib.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SoundsPath {

	private String d_Path = "";
	private String[] path_sounds;
	
	public SoundsPath(String path)  {
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				d_Path += reader.nextLine();
				path_sounds = d_Path.split("path_sound: ");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String[] getElement() {
		return path_sounds;
	}

}

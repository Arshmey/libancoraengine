package com.ancorastudio.LibAncoraEngine.AncoraEngine.engine.loader_lib.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ImagesPath {

	private String d_Path = "";
	private String[] path_images;
	
	public ImagesPath(String path) {
		try {
			Scanner reader = new Scanner(new File(path));
			while(reader.hasNext()) {
				d_Path += reader.nextLine();
				path_images = d_Path.split("path_img: ");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String[] getElement() {
		return path_images;
	}

}

package AncoraEngine.loader;

import java.io.File;

import AncoraEngine.engine.loader_lib.reader.ImagesPath;
import AncoraEngine.engine.loader_lib.reader.LocalizationPath;
import AncoraEngine.engine.loader_lib.reader.SoundsPath;
import AncoraEngine.engine.loader_lib.writer.Image;
import AncoraEngine.engine.loader_lib.writer.Localization;
import AncoraEngine.engine.loader_lib.writer.Sound;

public class Loader {
	
	private LocalizationPath loc_pth;
	private ImagesPath img_pth;
	private SoundsPath snd_pth;
	
	public Localization loc;
	public Image img;
	public Sound snd;
	
	public Loader(String path_localizer, int lang, String path_images, String path_sound) {

		if(new File(path_localizer).exists()) { 
			loc_pth = new LocalizationPath(path_localizer); 
				if(new File(loc_pth.getElement()[lang]).exists()) { loc = new Localization(loc_pth.getElement()[1]); } 
				else { System.out.println("Localization not exist"); }				
		} else { System.out.println("Localization path not exist"); }
			
		if(new File(path_images).exists()) { 
			img_pth = new ImagesPath(path_images);
			if(new File(img_pth.getElement()[1]).exists()) { img = new Image(img_pth.getElement()[1]); }
			else { System.out.println("Images not exist"); }
		} else { System.out.println("Images path not exist"); }
		
		if(new File(path_sound).exists()) {
			snd_pth = new SoundsPath(path_sound); 
			if(new File(snd_pth.getElement()[1]).exists()) { snd = new Sound(snd_pth.getElement()[1]); }
			else { System.out.println("Sounds not exist"); }
		} else { System.out.println("Sounds path not exist"); }
		
	}
}
